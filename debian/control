Source: jzmq
Section: java
Priority: optional
Maintainer: Jan Niehusmann <jan@debian.org>
Build-Depends: debhelper (>= 9), dh-exec (>=0.3), libzmq5-dev, default-jdk-headless (>= 2:1.8) | openjdk-8-jdk-headless | java-8-sdk-headless, libpgm-dev, pkg-config, libtool, autoconf, libtool-bin, automake, maven-repo-helper, javahelper, dh-autoreconf, default-jdk-doc, junit4, cppzmq-dev
Standards-Version: 3.9.6
Homepage: https://github.com/zeromq/jzmq
Vcs-Git: https://salsa.debian.org/jan/jzmq.git
Vcs-Browser: https://salsa.debian.org/jan/jzmq

Package: libzmq-java
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${java:Recommends}, libzmq-jni (= ${binary:Version})
Description: ZeroMQ Java bindings (jzmq)
 ZeroMQ is a library which extends the standard socket interfaces with features
 traditionally provided by specialised messaging middleware products.
 .
 ZeroMQ sockets provide an abstraction of asynchronous message queues, multiple
 messaging patterns, message filtering (subscriptions), seamless access to
 multiple transport protocols and more.
 .
 This package contains JNI-based Java bindings for ZeroMQ.

Package: libzmq-jni
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}, ${java:Recommends}
Pre-Depends: ${misc:Pre-Depends}
Description: ZeroMQ Java bindings (jzmq)
 ZeroMQ is a library which extends the standard socket interfaces with features
 traditionally provided by specialised messaging middleware products.
 .
 ZeroMQ sockets provide an abstraction of asynchronous message queues, multiple
 messaging patterns, message filtering (subscriptions), seamless access to
 multiple transport protocols and more.
 .
 This package contains JNI libraries for libzmq-java.

Package: libzmq-java-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, ${java:Recommends}
Description: Documentation for ZeroMQ Java bindings (jzmq)
 ZeroMQ is a library which extends the standard socket interfaces with features
 traditionally provided by specialised messaging middleware products.
 .
 ZeroMQ sockets provide an abstraction of asynchronous message queues, multiple
 messaging patterns, message filtering (subscriptions), seamless access to
 multiple transport protocols and more.
 .
 This package contains JavaDoc documentation for the Java bindings for ZeroMQ.

